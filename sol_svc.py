"""
This code is used for solving the problem using SVC
"""

# Make necessary imports
from sklearn import svm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import warnings
from sklearn import preprocessing


###########
#Main Function
if __name__ == "__main__":
	warnings.filterwarnings("ignore")

	#Read the data from sonar.all-data file
	data=pd.read_csv("sonar.all-data")

	label_encoder = preprocessing.LabelEncoder()
	data.iloc[:,60]= label_encoder.fit_transform(data.iloc[:,60])
	#print(data.iloc[:,60].unique())

	#x includes all the columns in data except the last one
	x=data.iloc[:,:-1]
	#y just includes the last column in data 
	#y tells whether the sonar signals bounced off a metal cylinder or off a roughly cylindrical rock
	y = data.iloc[:,-1]
	
	from sklearn.model_selection import train_test_split
	# Split data into training and test sets train size = 75% and test size = 25%
	X_train, X_test, y_train, y_test = train_test_split(x, y, test_size = 0.25, random_state = 21)

	# Fitting Kernel SVM to the Training set
	from sklearn.svm import SVC
	classifier = SVC(kernel = 'linear', random_state = 21)
	classifier.fit(X_train, y_train)
	
	# Predicting the Test set results
	y_pred = classifier.predict(X_test)
	
	# Making the Confusion Matrix
	from sklearn import metrics
	cm = metrics.confusion_matrix(y_test, y_pred)
	
	#Calculate accuracy
	print("Results without Hyperparamter Tuning:")
	accuracy=metrics.accuracy_score(y_test,y_pred)
	print("Accuracy: ", accuracy)

	# Making the Confusion Matrix
	cm = metrics.confusion_matrix(y_test, y_pred)
	print("Confusion Matrix: ", cm)
	
	#Precision
	print("Precision:",metrics.precision_score(y_test, y_pred))
	
	#Recall	
	print("Recall:",metrics.recall_score(y_test, y_pred))
	
	#Cross Validation Score
	from sklearn.model_selection import cross_val_score
	val_score_svc = cross_val_score(classifier, X_test, y_test, cv = 10)
	accu_svc = val_score_svc.mean()
	print("Cross validation for SVC is : ", accu_svc)


	#Hyperparameter tuning
	from sklearn.model_selection import GridSearchCV
	parameters = [{'C': [1, 10, 100, 1000], 'kernel': ['linear'], 'gamma' : ['auto']}, 
				 {'C': [1, 10, 100, 1000], 'kernel': ['rbf'], 'gamma': [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]},
				 {'C': [1, 10, 100, 1000], 'kernel': ['poly'], 'degree': [0, 1, 2, 3, 4, 5, 6], 'gamma' : ['auto']}]

	grid_search = GridSearchCV(estimator = classifier,
							   param_grid = parameters,
							   scoring = 'accuracy',
							   cv = 10,
							   n_jobs = -1)
	grid_search = grid_search.fit(X_train, y_train)
	
	print("\n \n")
	print("Results after Hyperparmater Tuninig: ")
	#print("Accuracy: ", grid_search.best_score_)
	print("Case parameters: ", grid_search.best_params_)
	
	#Finding the actual accuracy (Above accuracy is Grid Search Accuracy) 
	classifier = SVC(kernel = 'rbf', C=10, gamma=0.6)
	classifier.fit(X_train, y_train)
	
	# Predicting the Test set results
	y_pred = classifier.predict(X_test)

	# Making the Confusion Matrix
	cm = metrics.confusion_matrix(y_test, y_pred)
	print("Confusion Matrix: ", cm)
	
	#Accuracy
	print("Actual accuracy: ", metrics.accuracy_score(y_test,y_pred))

	#Precision
	print("Precision:",metrics.precision_score(y_test, y_pred))
	
	#Recall	
	print("Recall:",metrics.recall_score(y_test, y_pred))
	
	#Cross Validation Score
	from sklearn.model_selection import cross_val_score
	val_score_svc = cross_val_score(classifier, X_test, y_test, cv = 10)
	accu_svc = val_score_svc.mean()
	print("Cross validation for SVC is : ", accu_svc)

