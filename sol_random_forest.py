"""
This code is used for solving the problem using Random Forest Technique
"""

# Make necessary imports
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.metrics import roc_curve, auc, precision_score, recall_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn import preprocessing
import numpy as np
import warnings

###########
#Main Function
if __name__ == "__main__":
	warnings.filterwarnings("ignore")

	#Read the data from sonar.all-data file
	data=pd.read_csv("sonar.all-data")

	label_encoder = preprocessing.LabelEncoder()
	data.iloc[:,60]= label_encoder.fit_transform(data.iloc[:,60])

	#x includes all the columns in data except the last one
	x=data.iloc[:,:-1]
	#y just includes the last column in data 
	#y tells whether the sonar signals bounced off a metal cylinder or off a roughly cylindrical rock
	y = data.iloc[:,-1]
	
	# Split data into training and test sets train size = 75% and test size = 25%
	X_train, X_test, y_train, y_test = train_test_split(x, y, test_size = 0.25, random_state = 1)

	print("Results without Hyperparamater tuning: ")
	# Fitting Random Forest Classifier to the Training set
	from sklearn.ensemble import RandomForestClassifier
	classifier = RandomForestClassifier(random_state = 21)
	classifier.fit(X_train, y_train)
	
	# Predicting the Test set results
	y_pred = classifier.predict(X_test)

	#Calculate accuracy
	accuracy=accuracy_score(y_test,y_pred)
	print("Accuracy: ", accuracy)
	
	#Precision
	print("Precision:",precision_score(y_test, y_pred))
	
	#Recall	
	print("Recall:",recall_score(y_test, y_pred))
	
	#Cross Validation Score
	val_score_svc = cross_val_score(classifier, X_test, y_test, cv = 10)
	accu_svc = val_score_svc.mean()
	print("Cross validation for SVC is : ", accu_svc)
	
	# Making the Confusion Matrix
	cm = confusion_matrix(y_test, y_pred)
	print("Confusion Matrix: ",cm)

	#We will use AUC (Area Under Curve) as the evaluation metric. 
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, y_pred)
	roc_auc = auc(false_positive_rate, true_positive_rate)
	print("Area under curve: ", roc_auc)
	
	#Applying Grid Search to find the best model and the best parameters
	parameters = [{'n_estimators' : [50, 100, 200], 
					'max_depth' : np.linspace(40, 70, 3, endpoint=True),
					'min_samples_split' : np.linspace(0.1, 1.0, 2, endpoint=True), 
					'min_samples_leaf' : np.linspace(0.1, 0.5, 2, endpoint=True)}]
	
	
	grid_search = GridSearchCV(estimator = classifier,
							   param_grid = parameters,
							   scoring = 'roc_auc',
							   cv = 10,
							   n_jobs = -1)
	grid_search = grid_search.fit(X_train, y_train)
	
	print("\n \n")
	print("Results after Hyperparamter Tuning: ")
	print("Case parameters: ", grid_search.best_params_)
	
	classifier = RandomForestClassifier(n_estimators=100,max_depth=70, random_state = 21)
	classifier.fit(X_train, y_train)
	
	# Predicting the Test set results
	y_pred = classifier.predict(X_test)

	#Calculate accuracy
	accuracy=accuracy_score(y_test,y_pred)
	print("Accuracy: ", accuracy)
	
	#Precision
	print("Precision: ",precision_score(y_test, y_pred))
	
	#Recall	
	print("Recall: ",recall_score(y_test, y_pred))
	
	#Cross Validation Score
	val_score_svc = cross_val_score(classifier, X_test, y_test, cv = 10)
	accu_svc = val_score_svc.mean()
	print("Cross validation for SVC: ", accu_svc)
	
	# Making the Confusion Matrix
	cm = confusion_matrix(y_test, y_pred)
	print("Confusion Matrix: ",cm)

	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, y_pred)
	roc_auc = auc(false_positive_rate, true_positive_rate)
	print("Area under curve: ", roc_auc)
