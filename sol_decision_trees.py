"""
This code is used for solving the problem using Decision Trees
"""

# Make necessary imports
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn.model_selection import cross_val_score


###########
#Main Function
if __name__ == "__main__":
	#Read the data from sonar.all-data file
	data=pd.read_csv("sonar.all-data", header= None)

	#x includes all the columns in data except the last one
	x=data.iloc[:,:-1]

	#y just includes the last column in data 
	#y tells whether the sonar signals bounced off a metal cylinder or off a roughly cylindrical rock
	y = data.iloc[:,-1]
	
	# Split data into training and test sets. Train size = 75% and Test size = 25%
	X_train, X_test, y_train, y_test = train_test_split(x, y, test_size = 0.25, random_state = 21, shuffle= True)
	
	#####Decision Tree Classifier using Gini as the criterion
	print("Results of Decision Tree Classifier using Gini as the criterion: ")	
	classifier_gini = DecisionTreeClassifier(criterion = "gini", random_state = 21, max_depth=3)
	classifier_gini.fit(X_train, y_train)
	
	#Accuracy for train data
	y_train_pred_gini = classifier_gini.predict(X_train)
	print ("Accuracy for train data: ", accuracy_score(y_train,y_train_pred_gini)*100)

	#Accuracy for test data
	Y_pred_gini = classifier_gini.predict(X_test)
	print ("Accuracy for test data ", accuracy_score(y_test,Y_pred_gini)*100)
	
	#Cross validation score
	val_score_gini = cross_val_score(classifier_gini, X_test, y_test, cv = 3)
	accu_gini = val_score_gini.mean()
	print("Cross validation score : ", accu_gini)

	#####Decision Tree Classifier using Entropy as the criterion
	print("Results of Decision Tree Classifier using Entropy as the criterion: ")
	classifier_entropy= DecisionTreeClassifier(criterion = "entropy", random_state = 21, max_depth=3)
	classifier_entropy.fit(X_train, y_train)

	#Accuracy for train data
	y_train_pred_entropy = classifier_entropy.predict(X_train)
	print ("Accuracy for train data: ", accuracy_score(y_train,y_train_pred_entropy)*100)
	
	#Accuracy for test data
	Y_pred_entropy = classifier_entropy.predict(X_test)
	print ("Accuracy for test data: ", accuracy_score(y_test,Y_pred_entropy)*100)
	
	#Cross validation score
	val_score_entropy = cross_val_score(classifier_entropy, X_test, y_test, cv = 3)
	accu_entropy = val_score_entropy.mean()
	print("cross validation score: ", accu_entropy)

