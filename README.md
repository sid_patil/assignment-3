The solution to assignment 3 consists of 3 Python Scripts.
sol_decision_trees.py -> Uses Decision Trees for solving the problem.
sol_random_forest.py -> Uses Random Forest for solving the problem.
sol_svc.py -> Uses Support Vector Clustering for solving the problem.

Following are the requirements for running the Python Scripts.
	Data file:
		sonar.all-data

	Python3 Libraries:
		sklearn
		numpy
		pandas
		matplotlib



